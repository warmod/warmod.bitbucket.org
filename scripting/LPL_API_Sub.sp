#pragma semicolon 1

#include <sourcemod>
#include <sdktools>
#include <SteamWorks>
#undef REQUIRE_PLUGIN
#include <updater>
#pragma newdecls required

#define KEY "APIKey" // Add API key here
#define UPDATE_URL    "https://warmod.bitbucket.io/updatefile_Subscription.txt" // Change to update file URL to auto update plugins
#define CHAT_PREFIX "LPL"

bool bLateLoad = false;
char NetIP[32];
char NetPort[10];

public Plugin myinfo = {
	name        = "Check Player Subscription",
	author      = "Versatile_BFG",
	description = "",
	version     = "0.1",
	url         = ""
};

public void OnPluginStart() {
	
	if (LibraryExists("updater")) {
        Updater_AddPlugin(UPDATE_URL);
    }
	
	int pieces[4];
	int longip = GetConVarInt(FindConVar("hostip"));
	pieces[0] = (longip >> 24) & 0x000000FF;
	pieces[1] = (longip >> 16) & 0x000000FF;
	pieces[2] = (longip >> 8) & 0x000000FF;
	pieces[3] = longip & 0x000000FF;
	Format(NetIP, sizeof(NetIP), "%d.%d.%d.%d", pieces[0], pieces[1], pieces[2], pieces[3]);
	Format(NetPort, sizeof(NetPort), "%i", GetConVarInt(FindConVar("hostport")));
	
	if (bLateLoad) {
		for (int i=1; i<=MaxClients; i++) {	
			if (IsClientInGame(i)) {
				if (!IsFakeClient(i)) {
					char auth_id[18];
					char ip[16];
					GetClientAuthId(i, AuthId_SteamID64, auth_id, sizeof(auth_id));
					GetClientIP(i, ip, sizeof(ip));
					SendPlayerInfo(i, auth_id, sizeof(auth_id), ip, sizeof(ip));
				}
			}
		}
	}
}

public void OnLibraryAdded(const char[]name) {
	if (StrEqual(name, "updater")) {
		Updater_AddPlugin(UPDATE_URL);
	}
}

public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max) {
	//... code here ...
	//RegPluginLibrary("LPL_Sub");
	bLateLoad = late;
	return APLRes_Success;
}


public void OnClientPostAdminCheck(int client) {
	if (client == 0) {
		return;
	}
	
	if (IsFakeClient(client)) {	
		return;
	}
	
	char auth_id[18];
	char ip[16];
	GetClientAuthId(client, AuthId_SteamID64, auth_id, sizeof(auth_id));
	GetClientIP(client, ip, sizeof(ip));
	SendPlayerInfo(client, auth_id, sizeof(auth_id), ip, sizeof(ip));
}

public void SendPlayerInfo(int client, char[] auth_id, int auth_size, char[] ip, int ip_size) {
	//steamworks//
	Handle request = SteamWorks_CreateHTTPRequest(k_EHTTPMethodPOST, "http://abc.com/api.php?p=ac"); // Change to API URL
	SteamWorks_SetHTTPRequestGetOrPostParameter(request, "action", "SendPlayerInfo"); // Change to API Action
	SteamWorks_SetHTTPRequestGetOrPostParameter(request, "api_key", KEY);
	SteamWorks_SetHTTPRequestGetOrPostParameter(request, "ip", NetIP);
	SteamWorks_SetHTTPRequestGetOrPostParameter(request, "port", NetPort);
	SteamWorks_SetHTTPRequestGetOrPostParameter(request, "player_ip", ip);
	SteamWorks_SetHTTPRequestGetOrPostParameter(request, "steam_id", auth_id);
	
	SteamWorks_SetHTTPCallbacks(request, OnGetPlayerMemberInfoComplete);
	SteamWorks_SetHTTPRequestContextValue(request, client);
	
	SteamWorks_SendHTTPRequest(request);
}

public int OnGetPlayerMemberInfoComplete(Handle request, bool bIOFailure, bool successful, EHTTPStatusCode status, any client) {
	if (successful) {
		int m_iLength = 2048;
		SteamWorks_GetHTTPResponseBodySize(request, m_iLength);
		char[] m_szBuffer = new char[m_iLength+1];
		SteamWorks_GetHTTPResponseBodyData(request, m_szBuffer, m_iLength);
		
		char data[PLATFORM_MAX_PATH];
		Format(data, sizeof(data), "%s", m_szBuffer);
		StripString(data, sizeof(data));
		LogAction(-1, -1, "%s", data);
		
	} else {
		LogError("SteamWorks error (status code %i). Request successful: %s", view_as<int>(status), successful ? "True" : "False");
	}
	
	CloseHandle(request);
}

stock void StripString(char[] filename, int size) {
	ReplaceString(filename, size, "{", "");
	ReplaceString(filename, size, "}", "");
	ReplaceString(filename, size, "\"", "");
	//"
}