#!/bin/bash

rm -Rf mm_sm
mkdir mm_sm && cd mm_sm

MM1="http://www.metamodsource.net/mmsdrop/1.11/"
MM2=$(wget https://mms.alliedmods.net/mmsdrop/1.11/mmsource-latest-linux -q -O -)
wget $MM1$MM2
#wget https://s3-ap-southeast-2.amazonaws.com/cloudstrike/mods/metamod.tar.gz

SM1="http://www.sourcemod.net/smdrop/1.11/"
SM2=$(wget https://sm.alliedmods.net/smdrop/1.11/sourcemod-latest-linux -q -O -)
wget $SM1$SM2
#wget https://s3-ap-southeast-2.amazonaws.com/cloudstrike/mods/sourcemod.tar.gz

wget http://users.alliedmods.net/~kyles/builds/SteamWorks/SteamWorks-git132-linux.tar.gz

sleep 5

tar zxfv mm*
tar zxfv metamod*
tar zxfv source*
tar zxfv SteamWorks*

rm *.gz
rm addons/sourcemod/configs/*.*

cd addons/sourcemod/plugins
mv antiflood.smx disabled/
mv basechat.smx disabled/
mv basetriggers.smx disabled/
mv funcommands.smx disabled/
mv funvotes.smx disabled/
mv nextmap.smx disabled/
mv reservedslots.smx disabled/
mv sounds.smx disabled/
